
<div class="age-restriction-wrap">
  <?php echo $restriction_content?>
  
  <div class="row top30">
    <div class="col-md-12">
     <a href="javascript:;" class="age-accept btn btn-info"><?php echo t("I'M OVER 19")?></a>
    </div>
    <div class="col-md-12 top10">
      <a href="javascript:;" class="age-exit btn btn-default"><?php echo t("EXIT")?></a>
    </div>
  </div> <!--row-->
  
</div> <!--age-restriction-wrap-->