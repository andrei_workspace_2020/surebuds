<style>
@media screen and (max-width: 768px) {

    #parallax-wrap .search-wraps.center.menu-header>img {
        display: none;
    }

    .mobile-banner-wrap.relative .layer {
        display: none;
    }

    .mobile-banner-wrap.relative .mobile-banner {
        display: none;
    }
}
</style>
<div class="mobile-banner-wrap relative">
    <div class="layer"></div>
    <img class="mobile-banner"
        src="<?php echo empty($background)?assetsURL()."/images/b-2-mobile.jpg":uploadURL()."/$background"; ?>">
</div>

<div id="parallax-wrap" class="parallax-search parallax-menu bg-default">
    <div class="search-wraps center menu-header">
        <img class="logo-medium bottom15" src="<?php echo $merchant_logo;?>">
        <div class="mytable">
            <img src="/assets/images/nexmo_logo.png">
        </div>
        <div class="mytable py-20">
            <p class="fs-30"><?=Yii::t('default', 'THE BEST')?><br /><?=Yii::t('default', 'IN CANNIBIS')?></p>
        </div>
        <div class="mytable">
            <div class="address-search">
                <span class="map-marker"><i class="fa fa-map-marker"></i></span>
                <input id="address-search-input" class="form-control" type="text" placeholder="Enter an address">
                <button class="btn-address-search"><?=Yii::t('default', 'SHOP NOW');?></button>
            </div>
        </div>
    </div>
    <!--search-wraps-->
    <div class="free-delivery">
        <img src="/assets/images/distance.png">
        <span><?=Yii::t('default', 'FREE Delivery on orders overs $100')?></span>
    </div>
</div>
<!--parallax-container-->
<!-- <div id="menu-sidenav">
	<a href="javascript:void(0)" class="btn-sidenav-close"><i class="fa fa-close"></i></a>
	<img src="/assets/images/nexmo_logo.png">
	<ul>
		
	</ul>
</div> -->
<div class="sections section-menu section-grey2">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div id="promotionSlider" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#promotionSlider" data-slide-to="0" class="active"></li>
                        <li data-target="#promotionSlider" data-slide-to="1"></li>
                        <li data-target="#promotionSlider" data-slide-to="2"></li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <div class="item active">
                            <img src="/assets/images/promoSlider/exotic.png">
                        </div>

                        <div class="item">
                            <img src="/assets/images/promoSlider/honeybadger.png">
                        </div>

                        <div class="item">
                            <img src="/assets/images/promoSlider/promo.png">
                        </div>
                    </div>

                    <!-- Left and right controls -->
                    <a class="left carousel-control" href="#promotionSlider" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#promotionSlider" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
        <!--row-->
        <div class="row">
            <div class="col-md-12">
                <div class="section section-new-arrivals">
                    <div class="section-header">
                        <h2 class="text-uppercase text-bold">NEW ARRIVALS</h2>
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-8"></div>
                            <div class="col-md-2 text-right">
                                <span class="text-link">
                                    <span>SEE ALL</span>
                                    <i class="fa fa-arrow-right"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="section-content">
                        <div class="card-list flex-row">
                            <div class="card card-item">
                                <div class="card-header">
                                    <div class="flex-row align-items-center justify-content-between">
                                        <span>5STAR</span>
                                        <span>27.3%</span>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="card-body-content">
                                        <img src="/assets/images/new-arrivals/1.png" class="w-100">
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="text-center">
                                        <p class="product-price">$35.00</p>
                                        <p class="product-name text-bold fs-16">CBD Bottles</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card card-item">
                                <div class="card-header">
                                    <div class="flex-row align-items-center justify-content-between">

                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="card-body-content">
                                        <img src="/assets/images/new-arrivals/2.jpg" class="w-100">
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="text-center">
                                        <p class="product-price">$260.00</p>
                                        <p class="product-name text-bold fs-16">Back Og</p>
                                        <button class="btn btn-sm btn-round btn-primary">INDICA</button>
                                    </div>
                                </div>
                            </div>
                            <div class="card card-item">
                                <div class="card-header">
                                    <div class="flex-row align-items-center justify-content-between">
                                        <span></span>
                                        <span>500MG</span>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="card-body-content">
                                        <img src="/assets/images/new-arrivals/3.png" class="w-100">
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="text-center">
                                        <p class="product-price">$15.00</p>
                                        <p class="product-name text-bold fs-16">Mazar Sherif Hash</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card card-item">
                                <div class="card-header">
                                    <div class="flex-row align-items-center justify-content-between">
                                        <span>5STAR+</span>
                                        <span>25.7%</span>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="card-body-content">
                                        <img src="/assets/images/new-arrivals/4.jpg" class="w-100">
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="text-center">
                                        <p class="product-price">$30.00</p>
                                        <p class="product-name text-bold fs-16">Mango-Zombie Juice</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card card-item">
                                <div class="card-header">
                                    <div class="flex-row align-items-center justify-content-between">
                                        <span>5STAR</span>
                                        <span>24.8%</span>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="card-body-content">
                                        <img src="/assets/images/new-arrivals/5.jpg" class="w-100">
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="text-center">
                                        <p class="product-price">$280.00</p>
                                        <p class="product-name text-bold fs-16">Kerosene Cookies by Monster's</p>
                                        <button class="btn btn-sm btn-round btn-success">HYBRID</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="section section-featured-products">
                    <div class="section-header">
                        <h2 class="text-uppercase text-bold">FEATURED PRODUCTS</h2>
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-8 text-center">
                                <span>DISCOVER SOME OF OUR TOP-SELLING PRODUCTS</span>
                            </div>
                            <div class="col-md-2 text-right">
                                <span class="text-link">
                                    <span>SEE ALL</span>
                                    <i class="fa fa-arrow-right"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="section-content">
                        <div class="card-list flex-row">
                            <div class="card-item">
                                <div class="card-header">
                                    <div class="flex-row align-items-center justify-content-between">
                                        <span>AAA</span>
                                        <span>25.2%</span>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="card-body-content">
                                        <img src="/assets/images/featured-products/1.jpg" class="w-100">
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="text-center">
                                        <p class="product-price">$50.00</p>
                                        <p class="product-name text-bold fs-16">Shake</p>
                                        <button class="btn btn-sm btn-round btn-primary">INDICA</button>
                                    </div>
                                </div>
                            </div>
                            <div class="card-item">
                                <div class="card-header">
                                    <div class="flex-row align-items-center justify-content-between">
                                        <span>5STAR+</span>
                                        <span>23.6%</span>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="card-body-content">
                                        <img src="/assets/images/featured-products/2.jpg" class="w-100">
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="text-center">
                                        <p class="product-price">$100.00</p>
                                        <p class="product-name text-bold fs-16">Greenhouse Bazooka Joe</p>
                                        <button class="btn btn-sm btn-round btn-success">HYBRID</button>
                                    </div>
                                </div>
                            </div>
                            <div class="card-item">
                                <div class="card-header">
                                    <div class="flex-row align-items-center justify-content-between">
                                        <span>5STAR+</span>
                                        <span>21%</span>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="card-body-content">
                                        <img src="/assets/images/featured-products/3.jpg" class="w-100">
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="text-center">
                                        <p class="product-price">$280.00</p>
                                        <p class="product-name text-bold fs-16">Lime Pop Kush by 6ixotics</p>
                                        <button class="btn btn-sm btn-round btn-primary">INDICA</button>
                                    </div>
                                </div>
                            </div>
                            <div class="card-item">
                                <div class="card-header">
                                    <div class="flex-row align-items-center justify-content-between">
                                        <span>5STAR+</span>
                                        <span>23.7%</span>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="card-body-content">
                                        <img src="/assets/images/featured-products/4.jpg" class="w-100">
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="text-center">
                                        <p class="product-price">$280.00</p>
                                        <p class="product-name text-bold fs-16">Pink 2.0 by 6ixotics</p>
                                        <button class="btn btn-sm btn-round btn-primary">INDICA</button>
                                    </div>
                                </div>
                            </div>
                            <div class="card-item">
                                <div class="card-header">
                                    <div class="flex-row align-items-center justify-content-between">
                                        <span>AAA+</span>
                                        <span>13.7%</span>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="card-body-content">
                                        <img src="/assets/images/featured-products/5.jpg" class="w-100">
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="text-center">
                                        <p class="product-price">$120.00</p>
                                        <p class="product-name text-bold fs-16">Devil's Kush(Seedy)</p>
                                        <button class="btn btn-sm btn-round btn-primary">INDICA</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="section section-edibles">
                    <div class="section-header">
                        <h2 class="text-uppercase text-bold">EDIBLES</h2>
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-8"></div>
                            <div class="col-md-2 text-right">
                                <span class="text-link">
                                    <span>SEE ALL</span>
                                    <i class="fa fa-arrow-right"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="section-content">
                        <div class="card-list flex-row">
                            <div class="card-item">
                                <div class="card-header">
                                    <div class="flex-row align-items-center justify-content-between">
                                        <span></span>
                                        <span>200MG</span>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="card-body-content">
                                        <img src="/assets/images/edibles/1.jpg" class="w-100">
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="text-center">
                                        <p class="product-price">$10.00</p>
                                        <p class="product-name text-bold fs-16">Puffed Rice Milk Chocolate</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card-item">
                                <div class="card-header">
                                    <div class="flex-row align-items-center justify-content-between">
                                        <span></span>
                                        <span>250MG</span>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="card-body-content">
                                        <img src="/assets/images/edibles/2.jpg" class="w-100">
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="text-center">
                                        <p class="product-price">$20.00</p>
                                        <p class="product-name text-bold fs-16">250mg Indica Cookies and Green Shatter
                                            Bar by Euphoria Extractions</p>
                                        <button class="btn btn-sm btn-round btn-primary">INDICA</button>
                                    </div>
                                </div>
                            </div>
                            <div class="card-item">
                                <div class="card-header">
                                    <div class="flex-row align-items-center justify-content-between">
                                        <span></span>
                                        <span>500MG</span>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="card-body-content">
                                        <img src="/assets/images/edibles/3.jpg" class="w-100">
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="text-center">
                                        <p class="product-price">$40.00</p>
                                        <p class="product-name text-bold fs-16">500mg Indica Milk Chocolate Shatter Bar
                                            by Euphoria Extractions</p>
                                        <button class="btn btn-sm btn-round btn-primary">INDICA</button>
                                    </div>
                                </div>
                            </div>
                            <div class="card-item">
                                <div class="card-header">
                                    <div class="flex-row align-items-center justify-content-between">
                                        <span></span>
                                        <span>100MG</span>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="card-body-content">
                                        <img src="/assets/images/edibles/4.jpg" class="w-100">
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="text-center">
                                        <p class="product-price">$15.00</p>
                                        <p class="product-name text-bold fs-16">Sativa Cookies by Euphoria Extractions
                                        </p>
                                        <button class="btn btn-sm btn-round btn-warning">SATIVA</button>
                                    </div>
                                </div>
                            </div>
                            <div class="card-item">
                                <div class="card-header">
                                    <div class="flex-row align-items-center justify-content-between">
                                        <span></span>
                                        <span>100MG</span>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="card-body-content">
                                        <img src="/assets/images/edibles/5.jpg" class="w-100">
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="text-center">
                                        <p class="product-price">$15.00</p>
                                        <p class="product-name text-bold fs-16">Indica Cookies by Euphoria Extractions
                                        </p>
                                        <button class="btn btn-sm btn-round btn-primary">INDICA</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--container-->
</div>
<!--section-menu-->