<?PHP
$title = 'Administration Panel';
include('include/config.php');
echo '<link rel="stylesheet" href="css/css.css">';
if(!empty($_GET['act'])){
	$act = $_GET['act'];
	echo '<center><table class="admin" width="15%"><th><h2>Administration</h2></th><tr><td valign="center">';
	switch($act){
		case 'login':
			echo '<form action="?act=loginproc" method="post">';
			echo '<input type=text name=user placeholder="Username"/><br/><br/>';
			echo '<input type=password name=pass placeholder="Password"/><br/><br/>';
			echo '<input type=submit value="Login"/></form>';
		break;
		case 'loginproc':
			$sql = mysqli_query($conn,"SELECT id, name, pass FROM account WHERE name='".valid($_POST['user'])."'")or die(mysqli_error($conn));
			if(mysqli_num_rows($sql) > 0){
				$result = mysqli_fetch_array($sql);
				if(password_verify($_POST['pass'],$result['pass'])){
					$_SESSION['id'] = $result['id'];
					Header("Location: listview.php");
				}else{
					echo 'Email/Pass Wrong<br/><br/><input type="button" value="Go Back" onclick="history.go(-1)">';
				}
			}else{
				echo 'No Account<br/><br/><input type="button" value="Go Back" onclick="history.go(-1)">';
			}
		break;
		case 'logout':
			session_destroy();
			Header("Location: listview.php?act=login");
		break;
	}
	echo '</td></tr></table></center>';
}else{
	restrict();
	echo '<ul><li><a href="?page=home">Home</a></li><li><a href="?page=city">Cities</a></li><li><a href="?page=sales">Sales</a></li><li><a href="?page=search">Search</a></li><li><a href="?page=monthly">Monthly Summary</a></li><li><a href="?page=showall">Full Summary</a></li><li><a href="?act=logout">Logout</a></li></ul><div style="margin-left:10%;padding:1px 16px;height:1000px;">';
	if(!empty($_GET['page'])){$page = $_GET['page'];}else{$page = 'showall';}
	switch($page){
		case 'home':
			echo 'Please visit links on the side to view certain information';
		break;
		case 'sales':
			if(!empty($_GET['do']) && $_GET['do'] == 'save'){
				mysqli_query($conn,"UPDATE base_price SET base='".$_POST['cost']."' WHERE item_id='".$_GET['sid']."'")or die(mysqli_error($conn));
				Header("Location: ?page=sales");
			}else{
				$saleinv = array(1 => array(),2 => array());
				$listitem = mysqli_query($conn,"SELECT merchant_id, item_id FROM mt_item ORDER BY item_id ASC");
				while($list = mysqli_fetch_array($listitem)){
					$saleinv[$list['merchant_id']] += array($list['item_id'] => array('Single' => array(),'7' => array(),'14' => array(),'28' => array(),'Total' => array(),'Pack of 7' => array()));
				}
				$query = mysqli_query($conn,"SELECT order_id, json_details FROM mt_order WHERE status='Delivered'")or die(mysqli_error($conn));
				while($fet = mysqli_fetch_array($query)){
					$result = json_decode($fet['json_details'],true);
					foreach($result AS $key=>$value){
						list($mprice,$w,$unk) = explode('|', $value['price']);
						$w = str_replace("g","",$w);
						if($w == '1' || $w == 'Sinle'){
							array_push($saleinv[$value['merchant_id']][$value['item_id']]['Single'],$mprice.'|'.$value['qty']);
						}elseif($w == '7'){
							array_push($saleinv[$value['merchant_id']][$value['item_id']][$w],$mprice.'|'.$value['qty']);
						}elseif($w == 'Pack of 7'){
							array_push($saleinv[$value['merchant_id']][$value['item_id']][$w],$mprice.'|'.$value['qty']);
						}elseif($w == '14'){
							array_push($saleinv[$value['merchant_id']][$value['item_id']][$w],$mprice.'|'.$value['qty']);
						}elseif($w == '28'){
							array_push($saleinv[$value['merchant_id']][$value['item_id']][$w],$mprice.'|'.$value['qty']);
						}else{
							echo 'NOT ACCOUNTED FOR : '.$w.'<br/>';
						}
					}
				}
				$loc = array(1 => 'Windsor',2 => 'Vaughn');
				foreach($loc AS $key=>$value){
					echo '<h2>'.$value.'</h2>';
					$itemq = mysqli_query($conn,"SELECT a.item_name, a.item_id, a.photo, b.base FROM mt_item AS a, base_price AS b WHERE a.merchant_id='$key' AND a.item_id=b.item_id ORDER BY a.item_id ASC")or die(mysqli_error($conn));
					echo '<table id="customers"><tr id="nonhead"><td align="right">Strain ID</td><td width="500">Strain Name</td><td width="150">Sold(# of units)</td><td align="right">Total</td><td width="200" align="right">Base Cost (per Oz)</td><td align="right"># of ounces</td><td align="right">Grant Total</td><td align="right">Profit</td></tr>';
					$sold = 0;
					$tgtotal = $ttotal = 0;
					while($res = mysqli_fetch_array($itemq)){
						$totals = $grams = $gtotal = $profit = '';
						echo '<tr><td valign="top">'.$res['item_id'].'</td><td valign="top"><img src="upload/'.$res['photo'].'" width="50" height="50"/>'.$res['item_name'].'</td>';
						echo '<td valign="top">';
						$p = $q = array('Single' => 0,'7' => 0,'14' => 0,'28' => 0,'Pack of 7' => 0);
						foreach($saleinv[$key][$res['item_id']] as $key3 => $value3){
							if(count($saleinv[$key][$res['item_id']][$key3]) > 0){
								echo $key3.' : ';
								foreach($saleinv[$key][$res['item_id']][$key3] as $key4 => $value4){
									list($price,$qty) = explode('|', $value4);
									$p[$key3] += $price;
									$q[$key3] += $qty;
								}
								echo $q[$key3];
								echo '</br>';
							}
						}
						foreach($q as $qkey => $qvalue){
							if($qvalue > 0){
								$totals .= number_format($p[$qkey],2).'<br/>';
								$ttotal += $p[$qkey];
								if($qkey == 'Pack of 7'){
									$grams  .= number_format($q[$qkey] * 7 / 28,2).' oz<br/>';
									$gtotal .= number_format($res['base'] * $q[$qkey] * 7,2).'<br/>';
									$tgtotal += $res['base'] * $q[$qkey] * 7;
									$profit .= number_format($p[$qkey] - $res['base'] * $q[$qkey] * 7,2).'<br/>';
								}elseif($qkey == 'Single'){
									$grams  .= number_format($q[$qkey] * 1 / 28,2).' oz<br/>';
									$gtotal .= number_format($res['base'] * $q[$qkey],2).'<br/>';
									$tgtotal += $res['base'] * $q[$qkey];
									$profit .= number_format($p[$qkey] - $res['base'] * $q[$qkey],2).'<br/>';
								}else{
									$grams  .= number_format($q[$qkey] * $qkey / 28,2).' oz<br/>';
									$gtotal .= number_format($res['base']*($q[$qkey]*$qkey/28),2).'<br/>';
									$tgtotal += $res['base']*($q[$qkey]*$qkey/28);
									$profit .= number_format($p[$qkey] - $res['base'] * $q[$qkey] * $qkey / 28,2).'<br/>';
								}
							}
						}
						if($res['base'] > 0){
							$msg = ' bgcolor="green"';
						}else{
							$msg = ' bgcolor="red"';
						}

						echo '</td><td valign="top" align="right">'.$totals.'</td><td valign="top"'.$msg.'><form action="?page=sales&do=save&sid='.$res['item_id'].'" method="post"><input type=text name=cost value="'.$res['base'].'" size="8" maxlength="8"/><input type=submit value="update"/></form></td><td valign="top" align="right">'.$grams.'</td><td valign="top" align="right">'.$gtotal.'<br/></td><td valign="top" align="right">'.$profit.'<br/></td></tr>';
					}
					echo '<tr><td colspan="3">Grand Total</td><td align="right">Total</td><td colspan="2"></td><td align="right">Grand Total</td><td align="right">Profit</td></tr>';
					echo '<tr><td colspan="3"></td><td align="right">'.number_format($ttotal,2).'</td><td colspan="2"></td><td align="right">'.number_format($tgtotal,2).'</td><td align="right">'.number_format($ttotal-$tgtotal,2).'</td></tr>';
					echo '</table>';
				}
			}
		break;
		case 'city':
			$qu = mysqli_query($conn,"SELECT id, city, delivery_fee, merchant_id FROM mt_delivery_rate ORDER BY city ASC");
			echo '<table border="0" cellspacing="20">';
			$cnt = 1;
			while($fetch = mysqli_fetch_array($qu)){
				echo '<td><a href="?page=city&city='.$fetch['city'].'">'.$fetch['city'].'</a></td>';
				if($cnt >= 12){
					echo '</tr><tr>';
					$cnt = 0;
				}
				$cnt++;
			}
			echo '</table>';
			if(!empty($_GET['city'])){
				$city = valid($_GET['city']);
				$query = mysqli_query($conn,"SELECT b.city, a.delivery_date, a.order_id, a.sub_total, a.status, a.delivery_charge, a.total_w_tax, b.date_created, c.first_name, c.last_name, d.merchant_id FROM mt_order AS a, mt_order_delivery_address AS b, mt_client as c, mt_delivery_rate as d WHERE b.city=d.city AND a.order_id=b.order_id AND c.client_id=a.client_id AND (a.status='Delivered' OR a.status='delivered') AND b.city='$city' ORDER BY b.order_id DESC")or die(mysqli_error($conn));
				echo '<table id="customers"><tr id="nonhead"><td align="right">Order ID</td><td width="200">Name</td><td align="right">Sub Total</td><td align="right">Delivery</td><td align="right">Total</td><td>Date</td></tr>';
				while($fetch = mysqli_fetch_array($query)){
					echo '<tr><td align="right">'.$fetch['order_id'].'</td><td>'.$fetch['first_name'].' '.$fetch['last_name'].'</td><td align="right">'.(int)$fetch['sub_total'].'</td><td align="right">'.(int)$fetch['delivery_charge'].'</td><td align="right">'.(int)$fetch['total_w_tax'].'</td><td>'.$fetch['delivery_date'].'</td></tr>';
				}
				echo '</table>';
			}else{
				echo 'Select a city';
			}
		break;
		case 'monthly':
			if(!empty($_GET['do']) && $_GET['do'] == 'new'){
				mysqli_query($conn,"INSERT INTO expenses VALUES(NULL,'".valid($_POST['type'])."','".valid($_POST['name'])."','2019-".valid($_POST['start'])."','2019-".valid($_POST['end'])."','".valid($_POST['cost'])."')")or die(mysqli_error($conn));
				Header("Location: ?page=monthly");
			}else{
				echo '<h2>Income</h2><table id="customers"><tr id="nonhead"><td>City</td>';
				$month = array('January','February','March','April','May','June','July','August','September','October','November','December');
				foreach($month as $key){
					echo '<td>'.$key.'</td>';
				}
				echo '<td>Grand total</td></tr>';
				$mnth = 1;
				$qu = mysqli_query($conn,"SELECT city FROM mt_delivery_rate ORDER BY city ASC");
				$ftotal = 0;
				while($gcity = mysqli_fetch_array($qu)){
					$gtotal = 0;
					echo '<tr><td width="150">'.$gcity['city'].'</td>';
					while($mnth <= 12){
						$total = 0;
						$query = mysqli_query($conn,"SELECT b.city, a.delivery_date, a.order_id, a.sub_total, a.status, a.delivery_charge, a.total_w_tax, b.date_created, c.first_name, c.last_name, d.merchant_id, d.delivery_fee FROM mt_order AS a, mt_order_delivery_address AS b, mt_client as c, mt_delivery_rate as d WHERE b.city=d.city AND a.order_id=b.order_id AND c.client_id=a.client_id AND (a.status='Delivered' OR a.status='delivered') AND b.city='".$gcity['city']."' AND a.delivery_date LIKE '2019-".sprintf('%02d',$mnth)."-%'")or die(mysqli_error($conn));	
						while($fetch = mysqli_fetch_array($query)){
							if(($fetch['order_id'] < '635' && $fetch['sub_total'] >= '300') || ($fetch['order_id'] >= '635' && $fetch['sub_total'] >= '150')){
								$total += ($fetch['sub_total'] - $fetch['delivery_fee']);
							}else{
								$total += $fetch['sub_total'];
							}
						}
						echo '<td align="right" width="50">'.(int)$total.'</td>';
						$gtotal += $total;
						$mnth++;
					}
					$mnth = 1;
					$ftotal += $gtotal;
					echo '<td align="right">'.(int)$gtotal.'</td></tr>';
				}
				echo '<tr><td colspan="13">Final Total</td><td align="right">'.$ftotal.'</td></tr></table>';
				echo '<h2>Expenses</h2><table id="customers"><tr id="nonhead"><td>Expenses</td>';
				$month = array('January','February','March','April','May','June','July','August','September','October','November','December');
				foreach($month as $key){
					echo '<td>'.$key.'</td>';
				}
				echo '<td>Grand total</td></tr>';
				echo '<tr><td colspan="14"><form action="?page=monthly&do=new" method="POST"><input type=text name=name placeholder="Name of Item" size="15"/><input type=text name=cost placeholder="Cost" size="5"/><input type=radio name=type value="Recurring">Recurring <input type=radio name=type value="Single"/>Single <select name=start>';
				$mth = 1;
				foreach($month as $key){
					echo '<option value='.$mth.'>'.$key.'</option>';
					$mth++;
				}
				echo '</select><select name=end>';
				$mth = 1;
				foreach($month as $key){
					echo '<option value='.$mth.'>'.$key.'</option>';
					$mth++;
				}
				echo '<input type=submit value="Add"/></form></td></tr>';
				$gexpp = mysqli_query($conn,"SELECT name, type, start, end, cost FROM expenses ORDER BY id")or die(mysqli_error($conn));
				while($ge = mysqli_fetch_array($gexpp)){
					echo '<tr><td>'.$ge['name'].'</td>';
					$mnth = 1;
					$gtotal = 0;
					while($mnth <= 12){
						$total = 0;
						if($ge['type'] == 'Single'){
							list($syear,$smonth) = explode('-',$ge['start']);
							if($smonth == $mnth){
								$total = $ge['cost'];
								$gtotal += $total;
							}
						}elseif($ge['type'] == 'Recurring'){
							list($syear,$smonth) = explode('-',$ge['start']);
							list($eyear,$emonth) = explode('-',$ge['end']);
							if($mnth >= $smonth && $mnth <= $emonth){
								$total = $ge['cost'];
								$gtotal += $total;
							}
						}
						echo '<td>'.$total.'</td>';
						$mnth++;
					}
					echo '<td>'.$gtotal.'</td></tr>';
				}
				echo '</table>';
			}
		break;
		case 'search':
			if(!empty($_GET['day'])){
				$sch = "AND a.delivery_date='".valid($_GET['year'])."-".valid($_GET['month'])."-".valid($_GET['day'])."'";
				$day = $_GET['day'];
				$month = $_GET['month'];
				$year = $_GET['year'];
			}elseif(!empty($_GET['month'])){
				$sch = "AND a.delivery_date LIKE '".valid($_GET['year'])."-".valid($_GET['month'])."-%'";
				$day = 0;
				$month = $_GET['month'];
				$year = $_GET['year'];
			}elseif(!empty($_GET['year'])){
				$sch = "AND a.delivery_date LIKE '".valid($_GET['year'])."%'";
				$day = 0;
				$month = 0;
				$year = $_GET['year'];
			}else{
				$sch = '';$year = 0;$month = 0;$day = 0;
			}
			echo '<table cellpadding="10"><td>Year</td><td>Month</td><td>Day</td></tr><td valign="top"><table cellpadding="5"><tr><td valign="top"><a href="?page=search&year=2019">2019</a></td></tr></table></td><td valign="top">';
			if(!empty(valid($year))){
				$mcnt = 1;
				echo '<table cellpadding="5"><tr>';
				while($mcnt <= 12){
					if($month == $mcnt){
						$bgcolor = ' bgcolor="lightgreen"';
					}else{
						$bgcolor = '';
					}
					echo '<td width="15" height="15"'.$bgcolor.'><a href="?page=search&year='.$_GET['year'].'&month='.sprintf('%02d',$mcnt).'">'.$mcnt.'</a></td>';
					if($mcnt == 6){
						echo '</tr><tr>';
					}
					$mcnt++;
				}
				echo '</tr></table>';
			}else{
				echo 'Please select a year';
			}
			echo '</td><td valign="top">';
			function days_in_month($month, $year){return $month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year % 400 ? 28 : 29))) : (($month - 1) % 7 % 2 ? 30 : 31);}
			if(!empty(valid($month))){
				$dcnt = 1;
				echo '<table cellpadding="5"><tr>';
				while($dcnt <= days_in_month($_GET['month'],$_GET['year'])){
					if($day == $dcnt){
						$bgcolor = ' bgcolor="lightgreen"';
					}else{
						$bgcolor = '';
					}
					echo '<td width="15" height="15"'.$bgcolor.'><a href="?page=search&year='.$_GET['year'].'&month='.sprintf('%02d',$_GET['month']).'&day='.sprintf('%02d',$dcnt).'">'.$dcnt.'</a></td>';
					if($dcnt == 14 || $dcnt == 28){
						echo '</tr><tr>';
					}
					$dcnt++;
				}
				echo '</tr></table>';
			}else{
				echo 'Please select a month';
			}
			echo '</td></tr></table>';
			echo '</table><br/>';
			$query = mysqli_query($conn,"SELECT b.city, a.delivery_date, a.order_id, a.sub_total, a.status, a.delivery_charge, a.total_w_tax, b.date_created, c.first_name, c.last_name, d.merchant_id FROM mt_order AS a, mt_order_delivery_address AS b, mt_client as c, mt_delivery_rate as d WHERE b.city=d.city AND a.order_id=b.order_id AND c.client_id=a.client_id AND (a.status='Delivered' OR a.status='delivered') ".$sch." ORDER BY b.order_id DESC")or die(mysqli_error($conn));
			echo '<table id="customers"><tr id="nonhead"><td align="right">Order ID</td><td>City</td><td width="200">Name</td><td align="right">Sub Total</td><td align="right">Delivery</td><td align="right">Total</td><td>Date</td></tr>';
			if(mysqli_num_rows($query) > 0 && ($year > 0 || $month > 0 || $day > 0)){
				while($fetch = mysqli_fetch_array($query)){
					echo '<tr><td align="right">'.$fetch['order_id'].'</td><td>'.$fetch['city'].'</td><td>'.$fetch['first_name'].' '.$fetch['last_name'].'</td><td align="right">'.(int)$fetch['sub_total'].'</td><td align="right">'.(int)$fetch['delivery_charge'].'</td><td align="right">'.(int)$fetch['total_w_tax'].'</td><td>'.$fetch['delivery_date'].'</td></tr>';
				}
			}else{
				echo '<tr><td colspan="7"><center>No Results</center></td></tr>';
			}
			echo '</table>';
		break;
		case 'showall':
			$loc = array(0 => array('name' => 'Windsor','city' => array()),1 => array('name' => 'Vaughn','city' => array()));
			$qu = mysqli_query($conn,"SELECT city, delivery_fee, merchant_id FROM mt_delivery_rate ORDER BY city ASC");
			while($fee = mysqli_fetch_array($qu)){
				$loc[$fee['merchant_id']-1]['city'][$fee['city']] = array('orders' => 0,'subtotal' => 0,'delivery' => 0,'total' => 0,'dcharge' => $fee['delivery_fee']);
			}
			$query = mysqli_query($conn,"SELECT b.city, a.delivery_date, a.order_id, a.sub_total, a.status, a.delivery_charge, a.total_w_tax, b.date_created, c.first_name, c.last_name, d.merchant_id FROM mt_order AS a, mt_order_delivery_address AS b, mt_client as c, mt_delivery_rate as d WHERE b.city=d.city AND a.order_id=b.order_id AND c.client_id=a.client_id AND (a.status='Delivered' OR a.status='delivered') ORDER BY b.order_id")or die(mysqli_error($conn));
			while($fetch = mysqli_fetch_array($query)){
				if(array_key_exists($fetch['city'],$loc[$fetch['merchant_id']-1]['city'])){
					$loc[$fetch['merchant_id']-1]['city'][$fetch['city']]['orders']++;
					$loc[$fetch['merchant_id']-1]['city'][$fetch['city']]['subtotal'] += $fetch['sub_total'];
					$loc[$fetch['merchant_id']-1]['city'][$fetch['city']]['delivery'] += $fetch['delivery_charge'];
					$loc[$fetch['merchant_id']-1]['city'][$fetch['city']]['total'] += $fetch['total_w_tax'];
				}
			}
			foreach($loc as $key=>$value){
				echo '<h2>City Name: '.$loc[$key]['name'].'</h2><table><tr>';
				$cnt = 0;
				foreach($loc[$key]['city'] as $key2=>$value2){
					echo '<td><table border="1" width="150" id="customers"><th colspan="2"><a href="?page=city&city='.$key2.'"><font color="white">'.$key2.'</font></a></th><tr><td>Orders</td><td align="right">'.number_format((int)$loc[$key]['city'][$key2]['orders']).'</td></tr><tr><td>Sub</td><td align="right">'.number_format((int)$loc[$key]['city'][$key2]['subtotal']).'</td></tr><tr><td>Delivery</td><td align="right">'.number_format((int)$loc[$key]['city'][$key2]['delivery']).'</td></tr><tr><td>Total</td><td align="right">'.number_format((int)$loc[$key]['city'][$key2]['total']).'</td></tr></table></td>';
					$cnt++;
					if($cnt >= 6){
						echo '</tr><tr>';
						$cnt = 0;
					}
				}
				echo '</tr></table>';
			}
		break;
	}
	echo '</div>';
}
?>